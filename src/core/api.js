import axios from "axios";

export default axios.create({
  baseURL: "https://app1.arricano.ua/looks_inside/",
  responseType: "json"
});