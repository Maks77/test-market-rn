import React, {useState, useEffect} from 'react'
import { StyleSheet, Dimensions, SafeAreaView, View, Text, Button, FlatList, Image } from 'react-native'
import api from '../core/api'
import styled from 'styled-components'
import {ProductCard} from '../parts'
import Constants from 'expo-constants';



const window = Dimensions.get("window");
const screen = Dimensions.get("screen");

const HomeScreen = ({navigation}) => {
    
    const [products, setProducts] = useState([])

    const items = [
        {id: 0, name: 'name0'},
        {id: 1, name: 'name1'},
        {id: 2, name: 'name2'},
    ]

    const getData = () => {
        api.get('products_api?page=0&per_page=10&text=&shop_id=0&gender=0&mode=1').then(res => {
            console.log('res', res);
            // const prodArr = res.data.data.map(el => {
            //     el.img = 'https://picsum.photos/100/100';
            //     return el;
            // });
            // console.log('res_arr', prodArr);
            setProducts(() => {
                return res.data.data
            })
        })
    }

    useEffect(() => {
        console.log('useEffect');
        
        getData()
    }, []) 

    return (
        <SafeAreaView>
            {/* <Button title={'get'} onPress={getData}></Button> */}
            <View style={styles.vwList}>
                <FlatList
                    data={products}
                    renderItem={(el) => (
                        <ProductCard product={el}></ProductCard>
                    )}
                    numColumns={2}
                    keyExtractor={item => item.id}
                    contentInset={{bottom: 50}}
                ></FlatList>
            </View>
            
        </SafeAreaView>
        
    )
}

const CardContainer = styled.View`
    flex: 0.5;
    height: 200px;
    elevation: 2px;
    background: #fff;
    margin: 4px;
    max-width: ${screen.width / 2 - 8}px;
`

const ListContainer = styled.View`

`

const styles = StyleSheet.create({
    cardContainer: {
        flex: 0.5,
        height: 200,
        elevation: 2,
        backgroundColor: '#fff',
        margin: 4,
        maxWidth: (screen.width / 2) - 8
    },
    img: {
        width: 100,
        height: 100
    },
    vwList: {
        // height: window.height - 250 //adjust
    }
})

export default HomeScreen;

// const products = [
    //     {
    //         id: 0,
    //         name: 'dress',
    //         img: "https://picsum.photos/100/100",
    //         price: 239
    //     },
    //     {
    //         id: 2,
    //         name: 'dress',
    //         img: "https://picsum.photos/100/100",
    //         price: 239
    //     },
    //     {
    //         id: 3,
    //         name: 'dress',
    //         img: "https://picsum.photos/100/100",
    //         price: 239
    //     },
    //     {
    //         id: 4,
    //         name: 'dress',
    //         img: "https://picsum.photos/100/100",
    //         price: 239
    //     },
    //     {
    //         id: 5,
    //         name: 'dress',
    //         img: "https://picsum.photos/100/100",
    //         price: 239
    //     }
    // ]