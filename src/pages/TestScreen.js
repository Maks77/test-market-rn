import React from 'react'
import { View, Text, Button } from 'react-native'
import api from '../core/api'

const TestScreen = ({navigation}) => {

    const getData = () => {
        api.get('products_api?page=0&per_page=10&text=&shop_id=0&gender=0&mode=1').then(res => {
            console.log('res_test', res);
        })
    }

    const goHome = () => {
        navigation.navigate('HomeScreen')
    }

    return (
        <View>
            <Text>TestScreen</Text>
            <Button title={'click'} onPress={getData}></Button>
            <Button title={'go HOME'} onPress={goHome}></Button>
        </View>
    )
}

export default TestScreen;