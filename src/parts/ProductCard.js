import React from 'react'
import { StyleSheet, Dimensions, TouchableOpacity, View, Text, Image } from 'react-native'
import api from '../core/api'
import styled from 'styled-components'
import {BookmarkIcon} from '../components/'

import { Ionicons } from '@expo/vector-icons'; 

const screen = Dimensions.get("screen");

const ProductCard = ({product}) => {
    console.log('product', product)

    const bookmarkPressed = () => {
        console.log('bookmarkPressed');
    }

    const productPressed = () => {
        console.log('productPressed', product);
    }

    const imgLoadError = (e) => {
        console.log('imgLoadError', e);
    }

    return (
        <CardContainer onPress={productPressed} activeOpacity={0.7}>
            <ProductImage onError={imgLoadError} source={{uri: product.item.img}}></ProductImage>
            <TextContainer>
                <ProductNameText>{product.item.name}</ProductNameText>
                <ProductPrice>{product.item.price}$</ProductPrice>
            </TextContainer>
            <BookmarkIcon onPress={bookmarkPressed}></BookmarkIcon>
        </CardContainer>
    )
}

const ProductPrice = styled.Text`
    font-size: 12px;
`

const ProductNameText = styled.Text`
    font-size: 18px;
    font-weight: bold;
`

const TextContainer = styled.View`
    padding: 4px 8px;
`

const ProductImage = styled.Image`
    flex: 1;
`

const CardContainer = styled.TouchableOpacity`
    flex: 0.5;
    height: 200px;
    elevation: 2;
    background: #fff;
    margin: 8px;
    max-width: ${screen.width / 2 - 16}px;
    border-radius: 8px;
    overflow: hidden;
`

const styles = StyleSheet.create({
    cardContainer: {
        flex: 0.5,
        height: 200,
        elevation: 2,
        backgroundColor: '#fff',
        margin: 8,
        maxWidth: (screen.width / 2) - 16
    }
})


export default ProductCard;