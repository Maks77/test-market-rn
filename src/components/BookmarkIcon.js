import React from 'react'
import { TouchableOpacity } from 'react-native'
import styled from 'styled-components'

import { Ionicons } from '@expo/vector-icons'; 

const BookmarkIcon = ({onPress}) => {
    return (
        <BookmarkIconTouchable onPress={onPress} activeOpacity={0.5}>
            <Ionicons name="md-bookmark" size={16} color="black" />
        </BookmarkIconTouchable>
    )
}

const BookmarkIconTouchable = styled.TouchableOpacity`
    width: 40px;
    height: 40px;
    border-radius: 40px;
    position: absolute;
    top: 8px;
    right: 8px;
    background-color: #fff
    justify-content: center;
    align-items: center;
`

export default BookmarkIcon